# revkrig

`revkrig` implements the method in 

Giorgi, E. and Diggle, P.J. (2015). On the inverse geostatistical problem of inference on missing locations. *Spatial Statistics*, **11**, 35--44.


