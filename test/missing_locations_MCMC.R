set.seed(19660331)
library(geoR)
library(pdist)
library(mnormt)
# Total number of locations
n.tot <- 503

# Set of all locations
coords.tot <- cbind(runif(n.tot),runif(n.tot))

# Parameters
model = list(
    sigma2 = 1, # variance of the Gaussian process
    tau2 = 0.5, # variance of the nugget effets
    phi = 0.2, # scale of spatial correlation
    mu = 1 # mean
)

### simulate data on the points and a grid

simdata = revkrigsample(model, n.tot)

# Number of missing locations
n.exclude <- 3

# (random) selection of observations WITH locations
ind.sel <- sample(1:n.tot,n.tot-n.exclude)

# select highest, smallest, medianest:
imax = which.max(y.tot)
imin = which.min(y.tot)
imed = which(y.tot == median(y.tot))

ind.sel = (1:n.tot)[-c(imin, imed, imax)]
                    

y <- y.tot[ind.sel]
coords <- coords.tot[ind.sel,]

library(sf)

spdata = st_as_sf(data.frame(coords,Z=y), coords=c(1,2))


# selection of observations WITHOUT locations
coords.star <- coords[-ind.sel,] # missing locations
y.star <- y.tot[-ind.sel] # outcome at missing locations

start.values <- coords.star


mcmc = list(n.sim = 10000,
    burnin = 2000,
    thin = 8)

model = list(sigma2 = sigma2, # variance of the Gaussian process
    tau2 = tau2, # variance of the nugget effets
    phi = phi, # scale of spatial correlation
    mu = mu # mean
    )

control = list(
    ## Probability of switching location
    prob.switch = 0.7,
    ## Tuning parameters
    h1 = 0.0005,
    h2 = 0.00025
    )
message("Running mcmc")


sim  <- revkrigMCMC(y,y.star,coords,start.values,mcmc,model,
             control)

message("mcmc finished")

# Plotting the posterior distribution of the missing locations
plot(sim[,,1],cex=0.5,pch=20,xlim=c(0,1),ylim=c(0,1))
points(coords.star[1,1],coords.star[1,2],pch=20,col=2,cex=2)

plot(sim[,,2],cex=0.5,pch=20,xlim=c(0,1),ylim=c(0,1))
points(coords.star[2,1],coords.star[2,2],pch=20,col=2,cex=2)

plot(sim[,,3],cex=0.5,pch=20,xlim=c(0,1),ylim=c(0,1))
points(coords.star[3,1],coords.star[3,2],pch=20,col=2,cex=2)
